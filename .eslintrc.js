module.exports = {
  env: {
    browser: true, // Enable browser global variables and browser-specific rules
    es6: true, // Enable ES6 features
  },
  extends: [
    'eslint:recommended', // Use ESLint's recommended rules
  ],
  parserOptions: {
    ecmaVersion: 2018, // The version of ECMAScript syntax it will use
    sourceType: 'module',
  },
  rules: {
    // Code Quality and Best Practices
    'no-console': 'warn', // Warn against using console.log
    'no-unused-vars': 'warn', // Warn about unused variables
    'no-undef': 'error', // Treat undeclared variables as errors

    // Coding Style
    'indent': ['error', 2], // Enforce 2-space indentation
    'semi': ['error', 'always'], // Require semicolons at the end of statements
    'quotes': ['error', 'single'], // Enforce single quotes for strings
    'comma-spacing': 'error', // Require spaces after commas
    'comma-style': 'error', // Enforce consistent comma style
    'no-trailing-spaces': 'error', // Disallow trailing spaces
    'object-curly-spacing': ['error', 'always'], // Enforce spacing in object literals
  },
};
